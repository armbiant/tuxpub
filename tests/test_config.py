# -*- coding: utf-8 -*-

import pytest
from tuxpub.config import TuxConfig

bucket = "foo-bucket"
region = "us-west-1"
site_title = "Tuxpub"


@pytest.fixture
def mock_env_empty(monkeypatch):
    monkeypatch.delenv("S3_BUCKET", raising=False)
    monkeypatch.delenv("S3_REGION", raising=False)
    monkeypatch.delenv("SITE_TITLE", raising=False)
    monkeypatch.delenv("ROOT_INDEX_LISTING", raising=False)


@pytest.fixture
def mock_env_minimum(monkeypatch):
    monkeypatch.setenv("S3_BUCKET", bucket)
    monkeypatch.setenv("S3_REGION", region)
    monkeypatch.setenv("SITE_TITLE", site_title)
    monkeypatch.delenv("ROOT_INDEX_LISTING", raising=False)


def test_no_s3_bucket(mock_env_empty):
    with pytest.raises(AssertionError) as excinfo:
        config = TuxConfig()  # noqa: F841
    assert "Required env var S3_BUCKET not set" in str(excinfo.value)


def test_no_s3_region(mock_env_empty, monkeypatch):
    monkeypatch.setenv("S3_BUCKET", bucket)
    with pytest.raises(AssertionError) as excinfo:
        config = TuxConfig()  # noqa: F841
    assert "Required env var S3_REGION not set" in str(excinfo.value)


def test_minimum(mock_env_minimum, monkeypatch):
    config = TuxConfig()
    assert config.S3_BUCKET == bucket
    assert config.S3_REGION == region
    assert config.SITE_TITLE == site_title
    assert config.ROOT_INDEX_LISTING  # defaults to True
    assert isinstance(config.ROOT_INDEX_LISTING, bool)  # ensure it's boolean


@pytest.mark.parametrize(
    "input,result",
    [
        ("true", True),
        ("TRUE", True),
        ("True", True),
        ("yes", True),
        ("1", True),
        ("false", False),
        ("FALSE", False),
        ("False", False),
        ("no", False),
        ("0", False),
    ],
)
def test_root_index_listing(input, result, mock_env_minimum, monkeypatch):
    monkeypatch.setenv("ROOT_INDEX_LISTING", input)
    config = TuxConfig()
    assert config.ROOT_INDEX_LISTING == result
    assert isinstance(config.ROOT_INDEX_LISTING, bool)  # ensure it's boolean
